module.exports = {
  port: 8555,
  testrpcOptions: '-p 8555',
  norpc: false,
  testCommand: 'node --max-old-space-size=4096 ../node_modules/.bin/truffle test --network coverage',
  skipFiles: []
};
