# TokenSale Contracts for Selfii

# Overview

Before the token sale starts there is an identity verification phase, where donors that wish to contribute are added to a whitelist. Once that step is complete, a per user cap for all users will be determined (based on the target raise and the number of users). Once that is done the token sale contract will be deployed. 

The public token sale will start a some point in time determined at the time of deployment. It is a two phase token sale, in the first phase anyone on the whitelist can contribute any amount up to their individual cap (which is stored in the whitelist). 

24 hours after the token sale starts phase two starts. In this phase anyone on the whitelist can contribute any amount regardless of their individual caps (as long as it would not exceed the hard cap for the token sale).

e.g. Alice and Bob have individual caps of 100 ETH, for simplicity the total cap of the token sale is 200 ETH. Bob donates 50 EUR during the first 24 hours of the token sale, Alice contributes 100 ETH (she may not contribute more until the second phase).

There is now 50 ETH left to be bought by either Alice or Bob during phase two. Who gets to contribute is determined on a first-come, first-served basis.

## Components

### Main Contracts for Audit

| Contract | Description |
|----------|-------------|
| TokenSale | Handles receiving funds, issuing tokens, and forwarding funds |
| Vault | Storage of funds during the token sale, handles refunds and distribution to beneficiaries |
| Token | A mintable version of OpenZeppelin's StandardToken |
| Whitelist | Add and track users allowed to participate in the token sale |

The contracts in the `vendored` folder are included for reference only.

The `testing` folder contains mocks for testing

#### Safeguards

The TokenSale contract can be `paused` causing it to reject state-mutating calls to it. After this has been done refunds can be enabled in the vault via the `enableRefunds` method, to allow funds to be returned to donors.

The `TokenSale` contract has an `emergencyDrain` method allowing any Ether that was caught in it to be drained by the owner, when the token sale has been paused. There should never be any funds held by this contract as everything is forwarded.

Any tokens sent to `TokenSale` or `Vault` can be drained by the owner. For the `Vault` tokens can only be drained upon completion of the token sale as ownership is held by `TokenSale` up to that point.

## Installing Dependencies

Run `yarn` or `npm install`

## Running Tests with testrpc

To launch an instance of `testrpc` and start the , tests, run

```
yarn test
```

### Known issues

The testsuite sometimes fails because of a snapshot bug in testrpc.

Two tests for `Vault.close()` fails during coverage testing due `.transfer()` to each of the destination wallets throwing for an unknown reason (if sending to regular accounts it does not throw). 

This behaviour only happens with the coverage instrumentation (i.e. not when running tests normally).

## Running Tests with Dockerized Parity

### Setup
This will create and start a docker container running Parity in the foreground

```
cd parity_docker
sudo docker build -t parityselfii .
sudo docker run -t -p 127.0.0.1:8945:8945 --name parity_selfii parityselfii
```

Now switch to another terminal and execute in the selfii directory
```
yarn test-docker
```

to run the testsuite.

## Audits