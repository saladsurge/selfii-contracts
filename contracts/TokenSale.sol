pragma solidity ^0.4.18;

import "./Token.sol";
import "./interfaces/IWhitelist.sol";
import "./Vault.sol";
import "./vendored/zeppelin/Ownable.sol";
import "./vendored/zeppelin/HasNoTokens.sol";
import "./vendored/zeppelin/Pausable.sol";
import "./vendored/zeppelin/SafeMath.sol";


contract TokenSale is Ownable, HasNoTokens, Pausable {
  using SafeMath for uint256;
  Token public token;
  IWhitelist public whitelist;
  Vault public vault;
  address public constant beneficiaryA = 0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa;
  address public constant beneficiaryB = 0xbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab;
  uint256 public constant TOTAL_WEI_CAP = 195223 ether; // placeholder value
  uint256 public startTime;
  uint256 public uncappedStart;
  uint256 public endTime;
  uint256 public tokensPerWei;
  uint256 public weiReceived;
  bool public finalised;
  mapping (address => uint256) public donations;
  bool public refunding;

  event Donation(address from, uint256 amount);
  event Finalised();

  modifier saleActive() {
    require(saleStarted() && !saleEnded() && !finalised);
    _;
  }

  modifier setupDone() {
    require(token != address(0) && vault != address(0));
    _;
  }

  function TokenSale(
    address _whitelist,
    uint256 _start,
    uint256 _tokensPerWei
  ) public {
    require(_whitelist != address(0));
    require(_start >= getTime());
    require(_tokensPerWei > 0);

    whitelist = IWhitelist(_whitelist);
    startTime = _start;

    // uncapped phase starts 24 hours after the token sale does
    uncappedStart = _start.add(1 days);

    // after 30 days token sale will no longer accept funds
    // regardless of the amount raised
    endTime = _start.add(30 days);
    tokensPerWei = _tokensPerWei;
  }

  function setupToken() external onlyOwner {
    require(token == address(0));
    token = new Token(this); // deploy token and set self as minter

    // want to allow admin to drain tokens sent by mistake
    token.transferOwnership(msg.sender);
  }

  function setupVault() external onlyOwner {
    require(vault == address(0));
    vault = new Vault(beneficiaryA, beneficiaryB);
  }

  function getAvailableUncapped(uint256 weiAmount)
  internal constant returns (uint256)
  {
    if (weiReceived.add(weiAmount) > TOTAL_WEI_CAP) {
      return TOTAL_WEI_CAP.sub(weiReceived);
    }

    return weiAmount;
  }

  function getAvailableCapped(address donor, uint256 weiAmount, uint256 cap)
  internal constant returns (uint256)
  {
    uint256 available = cap.sub(donations[donor]);

    if (available > weiAmount) {
      available = weiAmount;
    }

    if (weiReceived.add(available) > TOTAL_WEI_CAP) {
      return TOTAL_WEI_CAP.sub(weiReceived);
    }

    return available;
  }

  function getAvailable(address donor, uint256 weiAmount) public constant returns (uint256) {
    if (!saleStarted() || saleEnded()) {
      return 0;
    }

    uint256 cap = whitelist.getCap(donor);

    if (cap == 0) {
      return 0;
    }

    // if phase 2 has started allow full amount as long as it is below total cap
    if (isPhaseTwo()) {
      return getAvailableUncapped(weiAmount);
    }

    // when in first phase, only allow user to donate up to his/her cap
    // user can donate multiple times
    return getAvailableCapped(donor, weiAmount, cap);
  }

  // Process a received donation in ETH, where the recipient of the tokens
  // created might be different from the donor address
  function donate(address recipient)
  public payable setupDone saleActive whenNotPaused returns (uint256)
  {
    require(recipient != address(0));

    uint256 weiAmount = getAvailable(recipient, msg.value);
    require(weiAmount > 0);

    uint256 numTokens = weiAmount.mul(tokensPerWei);
    weiReceived = weiReceived.add(weiAmount);
    donations[recipient] = donations[recipient].add(weiAmount);

    vault.deposit.value(weiAmount)(msg.sender);
    assert(token.mint(recipient, numTokens));

    // return ETH that could not be spent to sender
    if (weiAmount < msg.value) {
      msg.sender.transfer(msg.value.sub(weiAmount));
    }

    Donation(msg.sender, weiAmount);

    return numTokens;
  }

  // once paused and refunds are enabled there is no turning back
  // i.e. unpausing with refunds enabled would be bad
  function unpause() onlyOwner whenPaused public {
    require(!refunding);
    super.unpause();
  }

  function enableRefunds() onlyOwner whenPaused public {
    refunding = true;
    vault.enableRefunds();
  }

  function () public payable {
    donate(msg.sender);
  }

  function finalise() external whenNotPaused setupDone onlyOwner {
    require(canFinalise());
    finalised = true;
    token.finishMinting();
    vault.close();

    // vault is empty now, allow admin to drain tokens sent to it
    vault.transferOwnership(owner);
    Finalised();
  }

  // Allow admin to drain Ether that was caught in this contract after it being
  // paused, e.g. if someone targets this contract with selfdestruct
  function emergencyDrain(address recipient) external whenPaused onlyOwner {
    require(recipient != address(0));
    recipient.transfer(this.balance);
  }

  //////////////////////////
  // Utility functions
  //////////////////////////
  function canFinalise() internal constant returns (bool) {
    return weiReceived == TOTAL_WEI_CAP || saleEnded();
  }

  function saleEnded() public constant returns (bool) {
    return getTime() >= endTime;
  }

  function saleStarted() public constant returns (bool) {
    return getTime() >= startTime;
  }

  function isPhaseTwo() public constant returns (bool) {
    return getTime() >= uncappedStart;
  }

  function getTime() internal constant returns (uint256) {
    return now;
  }
}
