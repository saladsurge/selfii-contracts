pragma solidity ^0.4.18;

import "./vendored/zeppelin/Ownable.sol";


/*
  Slightly modified version of the kyber crowdsale whitelist
  * formatting
  * slackUserCap = publicCap
  * remove redundant timestamp in logentry
  * rename listUser(s) => addUser(s)
  * rename event to UserAdded
  * add explicit function visibility
*/
contract Whitelist is Ownable {
  uint public publicCap;
  mapping (address => uint) public addressCap;

  function Whitelist() public {}

  event UserAdded(address _user, uint _cap);

  // Owner can delist by setting cap = 0.
  // Owner can also change it at any time
  // assign a public sale user by setting cap to 1 wei
  // any other positive cap is a private sale user
  function addUser(address _user, uint _cap) public onlyOwner {
    addressCap[_user] = _cap;
    UserAdded(_user, _cap);
  }

  // an optimization in case of network congestion
  function addUsers(address[] _users, uint[] _cap) external onlyOwner {
    require(_users.length == _cap.length);

    for (uint i = 0; i < _users.length; i++) {
      addUser(_users[i], _cap[i]);
    }
  }

  function setPublicCap(uint _cap) external onlyOwner {
    publicCap = _cap;
  }

  function getCap(address _user) external constant returns (uint) {
    uint cap = addressCap[_user];

    if (cap == 1) {
      return publicCap;
    }

    return cap;
  }

  function destroy() external onlyOwner {
    selfdestruct(owner);
  }
}
