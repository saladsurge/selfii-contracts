pragma solidity ^0.4.18;

import "./vendored/zeppelin/HasNoTokens.sol";
import "./vendored/zeppelin/RefundVault.sol";


contract Vault is RefundVault, HasNoTokens {
  address secondaryWallet;
  uint256 public secondaryWalletProportion = 30;

  function Vault(address _wallet, address _secondaryWallet) public
  RefundVault(_wallet)
  {
    require(_secondaryWallet != address(0));
    secondaryWallet = _secondaryWallet;
  }

  function close() onlyOwner public {
    require(state == State.Active);
    state = State.Closed;

    uint256 secondarySlice = this.balance.mul(secondaryWalletProportion).div(100);
    uint256 walletSlice = this.balance.sub(secondarySlice);
    Closed();

    wallet.transfer(walletSlice);
    secondaryWallet.transfer(secondarySlice);

    // this should always be true at this point
    assert(this.balance == 0);
  }

  /* slightly modified version from parent class:
  * > assumption is that donors call this contract directly to ask for a refund
  * > sender can request a different receving address for refund, in case he is
  *   sent from a wallet that requires more gas than provided by .transfer()
  */
  function refund(address recipient) public {
    require(state == State.Refunding);
    require(deposited[msg.sender] > 0);
    uint256 depositedValue = deposited[msg.sender];
    deposited[msg.sender] = 0;
    recipient.transfer(depositedValue);
    Refunded(msg.sender, depositedValue);
  }
}
