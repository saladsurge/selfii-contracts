pragma solidity ^0.4.18;


contract Kamikaze {
  function Kamikaze() public {}
  function () public payable {}

  function die(address recipient) external {
    selfdestruct(recipient);
  }
}
