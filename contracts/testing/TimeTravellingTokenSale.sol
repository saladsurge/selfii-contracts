pragma solidity ^0.4.18;
import "../TokenSale.sol";


contract TimeTravellingTokenSale is TokenSale {
  uint256 public time = 1;

  function TimeTravellingTokenSale(
    address _whitelist,
    uint256 _start,
    uint256 _tokensPerWei
  ) TokenSale(_whitelist, _start, _tokensPerWei) public {
  }

  function getTime() internal constant returns (uint256) {
    return time;
  }

  function setVault(address _vault) external {
    vault = Vault(_vault);
  }

  function setToken(address _token) external {
    token = Token(_token);
  }

  function setTime(uint256 newTime) public {
    time = newTime;
  }

  function setWeiReceived(uint256 amount) external {
    require(amount <= TOTAL_WEI_CAP);
    weiReceived = amount;
  }
}
