pragma solidity ^0.4.18;
import "../Token.sol";


contract TimeTravellingToken is Token {
  uint64 public time = 1;

  function TimeTravellingToken(address _minter) public Token(_minter) {}

  function getTime() internal constant returns (uint64) {
    return time;
  }

  function setMinter(address _minter) public {
    minter = _minter;
  }

  function setTime(uint64 newTime) public {
    require(newTime > time);
    time = newTime;
  }
}
