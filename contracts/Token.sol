pragma solidity ^0.4.18;
import "./vendored/zeppelin/StandardToken.sol";
import "./vendored/zeppelin/HasNoTokens.sol";


contract Token is StandardToken, HasNoTokens {
  string public constant name = "Selfii Token";
  string public constant symbol = "SNAP";
  uint public constant decimals = 18;

  address public minter;
  bool public mintingFinished;

  event Mint(address recipient, uint256 amount);
  event MintFinished();

  modifier canMint() {
    require(msg.sender == minter && !mintingFinished);
    _;
  }

  modifier canTransfer(address from, address to, uint256 amount) {
    require(to != address(this) && to != address(0));
    require(mintingFinished);
    _;
  }

  function Token(address _minter) public {
    require(_minter != address(0));
    minter = _minter;
  }

  function mint(address _to, uint256 _amount) canMint public returns (bool) {
    totalSupply = totalSupply.add(_amount);
    balances[_to] = balances[_to].add(_amount);
    Mint(_to, _amount);
    return true;
  }

  function transfer(address _to, uint256 _value)
  canTransfer(msg.sender, _to, _value) public returns (bool)
  {
    return super.transfer(_to, _value);
  }

  function transferFrom(address _from, address _to, uint256 _value)
  canTransfer(_from, _to, _value) public returns (bool)
  {
    return super.transferFrom(_from, _to, _value);
  }

  function finishMinting() external canMint {
    mintingFinished = true;
    MintFinished();
  }
}
