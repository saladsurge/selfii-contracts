pragma solidity ^0.4.18;


contract IWhitelist {
  function getCap(address user) external constant returns (uint256);
  function enableRefunds() external;
}
