const Whitelist = artifacts.require('Whitelist');
const TokenSale = artifacts.require('TokenSale');

module.exports = function deployment(deployer) {
  const startTime = ((new Date().getTime()) / 1000) + 6;

  deployer.deploy(Whitelist).then(() =>
    deployer.deploy(TokenSale, Whitelist.address, startTime, 160)).then(
    async () => {
      const instance = TokenSale.at(TokenSale.address);
      await instance.setupToken();
      return instance.setupVault();
    });
};
