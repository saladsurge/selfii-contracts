const TokenSale = artifacts.require('TimeTravellingTokenSale');
const Vault = artifacts.require('Vault');
const Whitelist = artifacts.require('Whitelist');
const Token = artifacts.require('TimeTravellingToken');
const Kamikaze = artifacts.require('Kamikaze');
const utils = require('./utils');

const NULL_ADDRESS = '0x0000000000000000000000000000000000000000';

contract('TokenSale', (accounts) => {
  let whitelist;
  let instance;
  const startTime = 12;
  const endTime = startTime + (30 * 24 * 60 * 60);
  const tokensPerWei = 131;

  beforeEach(async () => {
    whitelist = await Whitelist.deployed();
    instance = await TokenSale.new(
      whitelist.address, startTime, tokensPerWei);
  });

  describe('setupToken()', () => {
    it('should throw if setupToken has already been done', async () => {
      await instance.setupToken();
      return utils.shouldThrow(instance.setupToken.bind(instance));
    });

    it('should throw if caller is not owner', () => (
      utils.shouldThrow(instance.setupToken.bind(instance, { from: accounts[1] }))));

    it('should create token and assign ownerships', async () => {
      const tokenBefore = await instance.token.call();
      assert.strictEqual(NULL_ADDRESS, tokenBefore);

      await instance.setupToken();

      const token = Token.at(await instance.token.call());
      assert.strictEqual(await token.owner.call(), accounts[0]);
      assert.strictEqual(await token.minter.call(), instance.address);
    });
  });

  describe('setupVault()', () => {
    it('should throw if caller is not owner', () => (
      utils.shouldThrow(instance.setupVault.bind(instance, { from: accounts[1] }))));

    it('should throw if vault has already been setup', async () => {
      await instance.setupVault();
      return utils.shouldThrow(instance.setupVault.bind(instance));
    });

    it('should set vault', async () => {
      await instance.setupVault();
      const vaultAddress = await instance.vault.call();
      assert.notStrictEqual(vaultAddress, NULL_ADDRESS);
    });
  });

  describe('getAvailable()', () => {
    const userCap = 12345;
    beforeEach(async () => {
      await whitelist.addUser(accounts[1], userCap);
      return instance.setTime(startTime);
    });

    it('should return zero when sale has not started', async () => {
      await instance.setTime(startTime - 1);
      const available = await instance.getAvailable.call(accounts[1], 100);
      assert.strictEqual(available.toNumber(), 0);
    });

    it('should return zero when sale has ended', async () => {
      await instance.setTime(endTime);
      const available = await instance.getAvailable.call(accounts[1], 100);
      assert.strictEqual(available.toNumber(), 0);
    });

    it('should return zero when user is not on whitelist', async () => {
      const available = await instance.getAvailable.call(accounts[2], 100);
      assert.strictEqual(available.toNumber(), 0);
    });

    describe('when in capped phase', () => {
      it('should return zero when user has already used his cap', async () => {
        await instance.setupVault();
        await instance.setupToken();
        await instance.donate(accounts[1], { value: userCap });
        const available = await instance.getAvailable.call(accounts[1], 100);
        assert.strictEqual(available.toNumber(), 0);
      });

      it('should return full amount when it is <= user cap', async () => {
        const availableEqual = await instance.getAvailable.call(accounts[1], userCap);
        const availableLess = await instance.getAvailable.call(accounts[1], 100);
        assert.strictEqual(availableEqual.toNumber(), userCap);
        assert.strictEqual(availableLess.toNumber(), 100);
      });

      it('should return less than cap when some of it has been used', async () => {
        await instance.setupVault();
        await instance.setupToken();
        await instance.donate(accounts[1], { value: userCap - 100 });
        const available = await instance.getAvailable.call(accounts[1], userCap);
        assert.strictEqual(available.toNumber(), 100);
      });

      it('should return zero when user cap would exceed total cap', async () => {
        await instance.setWeiReceived(await instance.TOTAL_WEI_CAP.call());
        const available = await instance.getAvailable.call(accounts[1], 150);
        assert.strictEqual(available.toNumber(), 0);
      });
    });

    describe('when in uncapped phase', () => {
      beforeEach(async () => {
        await instance.setTime(startTime + 86400);
      });

      it('should return full amount when it is less than cap', async () => {
        const totalCap = await instance.TOTAL_WEI_CAP.call();
        await instance.setWeiReceived(totalCap.minus(100));
        const available = await instance.getAvailable.call(accounts[1], 50);
        assert.strictEqual(available.toNumber(), 50);
      });

      it('should return smaller amount than requested when it would exceed total cap', async () => {
        const totalCap = await instance.TOTAL_WEI_CAP.call();
        await instance.setWeiReceived(totalCap.minus(100));

        const available = await instance.getAvailable.call(accounts[1], 150);
        assert.strictEqual(available.toNumber(), 100);
      });

      it('should return zero when total cap has been reached', async () => {
        await instance.setWeiReceived(await instance.TOTAL_WEI_CAP.call());
        const available = await instance.getAvailable.call(accounts[1], 150);
        assert.strictEqual(available.toNumber(), 0);
      });
    });
  });

  describe('donate()', () => {
    const userCap = 12345;

    beforeEach(async () => {
      await whitelist.addUser(accounts[1], userCap);
      await instance.setupToken();
      await instance.setupVault();
      return instance.setTime(startTime);
    });

    it('should throw when token is null', async () => {
      await instance.setToken(NULL_ADDRESS);
      return utils.shouldThrow(
        instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw when vault is null', async () => {
      await instance.setVault(NULL_ADDRESS);
      return utils.shouldThrow(
        instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw when recipient address is null', () => (
      utils.shouldThrow(instance.donate.bind(instance, 0, { value: 100 }))));

    it('should throw when available amount is zero', async () => {
      await instance.setWeiReceived(await instance.TOTAL_WEI_CAP.call());
      return utils.shouldThrow(instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw if user is not in whitelist', async () => (
      utils.shouldThrow(
        instance.donate.bind(instance, accounts[2], { value: 100 }))));

    it('should throw if sale has not started', async () => {
      await instance.setTime(startTime - 1);
      return utils.shouldThrow(instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw if sale has ended', async () => {
      await instance.setTime(endTime);
      return utils.shouldThrow(instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw if sale has been finalised', async () => {
      await instance.setTime(endTime);
      await instance.finalise();
      return utils.shouldThrow(instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should throw if sale has been paused', async () => {
      await instance.pause();
      return utils.shouldThrow(instance.donate.bind(instance, accounts[1], { value: 100 }));
    });

    it('should return ether that was not spent to sender', async () => {
      const balanceBefore = await web3.eth.getBalance(accounts[1]);
      const data = await instance.donate(
        accounts[1], { from: accounts[1], value: userCap + 150, gasPrice: 1 });
      const txCost = data.receipt.gasUsed;
      const balanceAfter = await web3.eth.getBalance(accounts[1]);
      assert.strictEqual(
        balanceAfter.toString(10),
        balanceBefore.minus(userCap).minus(txCost).toString(10));
    });

    it('should issue tokens and forward ether', async () => {
      const token = Token.at(await instance.token.call());
      const userBalanceBefore = await token.balanceOf.call(accounts[1]);
      const vaultBalanceBefore = await web3.eth.getBalance(await instance.vault.call());
      const amountToSpend = userCap - 150;

      await instance.donate(accounts[1], { value: amountToSpend });

      const userBalanceAfter = await token.balanceOf.call(accounts[1]);
      const vaultBalanceAfter = await web3.eth.getBalance(await instance.vault.call());
      const event = await utils.getLatestEvent(instance, 'Donation');
      assert.strictEqual(
        userBalanceAfter.toString(10),
        userBalanceBefore.plus(amountToSpend).mul(tokensPerWei).toString(10));
      assert.strictEqual(
        vaultBalanceAfter.toString(10),
        vaultBalanceBefore.plus(amountToSpend).toString(10));
      assert.strictEqual(event.from, accounts[0]);
      assert.strictEqual(event.amount.toString(10), amountToSpend.toString(10));
    });
  });

  describe('when receiving ether', () => {
    it('should process donation', async () => {
      const amountToSpend = 12345;
      await whitelist.addUser(accounts[0], amountToSpend);
      await instance.setTime(startTime);
      await instance.setupToken();
      await instance.setupVault();
      const available = await instance.getAvailable.call(accounts[0], amountToSpend);
      assert.strictEqual(
        available.toString(10),
        amountToSpend.toString());
      await instance.sendTransaction({
        from: accounts[0],
        value: amountToSpend,
      });
      const event = await utils.getLatestEvent(instance, 'Donation');
      assert.strictEqual(event.from, accounts[0]);
      assert.strictEqual(event.amount.toNumber(), amountToSpend);
    });
  });

  describe('finalise()', () => {
    beforeEach(async () => {
      await instance.setupVault();
      return instance.setupToken();
    });

    it('should throw if sale has been paused', async () => {
      await instance.setTime(endTime);
      await instance.pause();
      return utils.shouldThrow(instance.finalise.bind(instance));
    });

    it('should throw if token is null', async () => {
      await instance.setTime(endTime);
      await instance.setToken(NULL_ADDRESS);
      return utils.shouldThrow(instance.finalise.bind(instance));
    });

    it('should throw if vault is null', async () => {
      await instance.setTime(endTime);
      await instance.setVault(NULL_ADDRESS);
      return utils.shouldThrow(instance.finalise.bind(instance));
    });

    it('should throw if sale has not started', async () => {
      await instance.setTime(startTime - 1);
      return utils.shouldThrow(instance.finalise.bind(instance));
    });

    it('should throw if caller is not owner', async () => {
      await instance.setTime(endTime);
      return utils.shouldThrow(
        instance.finalise.bind(instance, { from: accounts[1] }));
    });

    it('should allow finalisation if endtime has been reached', async () => {
      await instance.setTime(endTime);
      const event = utils.getLatestEvent(instance, 'Finalised');
      assert.isDefined(event);
    });

    it('should allow finalisation as soon as cap has been reached', async () => {
      await instance.setWeiReceived(await instance.TOTAL_WEI_CAP.call());
      const event = utils.getLatestEvent(instance, 'Finalised');
      assert.isDefined(event);
    });

    it('should release funds, finish minting and transfer ownership of vault',
      async () => {
        await instance.setTime(startTime);
        await whitelist.addUser(accounts[1], 12345);
        await instance.donate(accounts[1], { value: 12345 });
        await instance.setTime(endTime);
        await instance.finalise();

        const vaultBalance = await web3.eth.getBalance(await instance.vault.call());
        assert.strictEqual(vaultBalance.toNumber(), 0);
        const token = Token.at(await instance.token.call());
        assert.isTrue(await token.mintingFinished.call());
        const vault = Vault.at(await instance.vault.call());
        const vaultOwner = await vault.owner.call();
        const owner = await instance.owner.call();
        assert.strictEqual(vaultOwner, owner);
      });
  });

  describe('unpause', () => {
    it('should throw if refunds were enabled', async () => {
      await instance.pause();
      await instance.setupVault();
      await instance.enableRefunds();
      return utils.shouldThrow(instance.unpause.bind(instance));
    });

    it('should throw if not paused', async () =>
      utils.shouldThrow(instance.unpause.bind(instance)));

    it('should throw if caller is not owner', async () => {
      await instance.pause();
      return utils.shouldThrow(
        instance.unpause.bind(instance, { from: accounts[1] }));
    });

    it('should set paused to false', async () => {
      await instance.pause();
      await instance.unpause();
      assert.isFalse(await instance.paused.call());
    });
  });

  describe('enableRefunds', () => {
    beforeEach(() => instance.setupVault());

    it('should throw when not paused', () =>
      utils.shouldThrow(instance.enableRefunds.bind(instance)));

    it('should throw when caller is not owner', async () => {
      await instance.pause();
      return utils.shouldThrow(
        instance.enableRefunds.bind(instance, { from: accounts[1] }));
    });

    it('should set refunding to true', async () => {
      await instance.pause();
      const stateBefore = await instance.refunding.call();
      assert.isFalse(stateBefore);
      await instance.enableRefunds();
      const stateAfter = await instance.refunding.call();
      assert.isTrue(stateAfter);
    });
  });

  describe('emergencyDrain', () => {
    beforeEach(() => instance.pause());

    it('should throw when caller is not owner', () =>
      utils.shouldThrow(instance.emergencyDrain.bind(
        instance, accounts[1], { from: accounts[1] })));

    it('should throw when given address is null', () =>
      utils.shouldThrow(instance.emergencyDrain.bind(instance, 0)));

    it('should throw when fundraiser is not paused', async () => {
      await instance.unpause();
      return utils.shouldThrow(
        instance.emergencyDrain.bind(instance, accounts[1]));
    });

    it('should transfer funds to given address', async () => {
      const kamikaze = await Kamikaze.new();
      await kamikaze.sendTransaction({ value: 1500 });
      await kamikaze.die(instance.address);
      const balance = await web3.eth.getBalance(instance.address);
      assert.strictEqual(balance.toNumber(), 1500);

      const balanceBefore = await web3.eth.getBalance(accounts[1]);
      await instance.emergencyDrain(accounts[1]);
      const balanceAfter = await web3.eth.getBalance(accounts[1]);
      assert.strictEqual(balanceAfter.minus(balanceBefore).toNumber(), 1500);
    });
  });
});

contract('TokenSale', () => {
  describe('constructor', () => {
    let whitelist;
    const startTime = 12;
    const tokensPerWei = 131;

    beforeEach(async () => {
      whitelist = await Whitelist.deployed();
    });

    it('should throw if whitelist is null address', () => (
      utils.shouldThrow(
        TokenSale.new.bind(
          TokenSale, NULL_ADDRESS, startTime, tokensPerWei))));

    it('should throw if start date is in the past', () => (
      utils.shouldThrow(
        TokenSale.new.bind(
          TokenSale, whitelist.address, 0, tokensPerWei))));

    it('should throw if tokens per wei is zero', () => (
      utils.shouldThrow(
        TokenSale.new.bind(
          TokenSale, whitelist.address, startTime, 0))));

    it('should not throw when arguments are valid', async () => {
      try {
        await TokenSale.new(whitelist.address, startTime, tokensPerWei);
      } catch (error) {
        assert.fail('should not have thrown');
      }
    });

    it('should assign startTime, endTime, tokensPerWei', async () => {
      try {
        const instance = await TokenSale.new(whitelist.address, startTime, tokensPerWei);
        const _start = await instance.startTime.call();
        const _end = await instance.endTime.call();
        const _rate = await instance.tokensPerWei.call();
        const _uncappedStart = await instance.uncappedStart.call();

        assert.strictEqual(_start.toNumber(), startTime);
        assert.strictEqual(_end.toNumber(), startTime + (86400 * 30));
        assert.strictEqual(_rate.toNumber(), tokensPerWei);
        assert.strictEqual(_uncappedStart.toNumber(), startTime + 86400);
      } catch (error) {
        assert.fail('should not have thrown');
      }
    });
  });
});
