const { assert } = require('chai');

async function getAllEvents(instance, eventName) {
  const watcher = instance[eventName]();
  const events = await new Promise((resolve, reject) => watcher.get(
    (error, result) => {
      watcher.stopWatching();
      if (error) {
        return reject(error);
      }
      return resolve(result);
    }));

  return events.map(event => event.args);
}

async function getLatestEvent(instance, eventName) {
  const events = await getAllEvents(instance, eventName);
  return events[events.length - 1];
}

async function usedAllGas(data) {
  const tx = await web3.eth.getTransaction(data.tx);
  return tx.gas === data.receipt.gasUsed;
}

async function shouldThrow(fn) {
  try {
    const receipt = await fn();

    // only portable way to do this right now
    // if we used all gas there's a high probability that the contract call
    // threw an exception
    if (await usedAllGas(receipt)) {
      throw new Error('invalid opcode');
    }

    assert.fail('should have thrown');
  } catch (error) {
    assert.oneOf(error.message, [
      'invalid opcode',
      'VM Exception while processing transaction: revert',
      'VM Exception while processing transaction: invalid opcode',
      "The contract code couldn't be stored, please check your gas amount.",
    ]);
  }
}

async function assertJump(error) {
  assert.include(error.message, 'invalid opcode');
}

module.exports = {
  getLatestEvent,
  getAllEvents,
  shouldThrow,
  assertJump,
};
