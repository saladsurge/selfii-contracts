const Vault = artifacts.require('Vault');
const Wallet = artifacts.require('MultiSigWallet');
const utils = require('./utils');

contract('Vault', (accounts) => {
  let instance;
  let spender1;
  let spender2;

  before(async () => {
    spender1 = await Wallet.new(accounts, 2);
    spender2 = await Wallet.new(accounts, 2);
  });

  beforeEach(async () => {
    instance = await Vault.new(spender1.address, spender2.address);
  });

  describe('deposit', () => {
    it('should throw if refunds have been enabled', async () => {
      await instance.enableRefunds();
      return utils.shouldThrow(
        instance.deposit.bind(instance, accounts[0], { value: 1 }));
    });

    it('should log deposit', async () => {
      await instance.deposit(accounts[0], { value: '3124123874698734592' });
      const amount = await instance.deposited.call(accounts[0]);
      assert.strictEqual(amount.toString(10), '3124123874698734592');
    });
  });

  describe('close', () => {
    it('should throw if caller is not owner', async () => {
      await instance.deposit(accounts[0], { value: 120 });
      return utils.shouldThrow(instance.close.bind(instance, { from: accounts[1] }));
    });

    it('should set state to closed', async () => {
      const stateBefore = await instance.state.call();
      assert.strictEqual(stateBefore.toString(10), '0');
      await instance.deposit(accounts[0], { value: 100 });
      await instance.close();
      const state = await instance.state.call();
      const event = await utils.getLatestEvent(instance, 'Closed');

      assert.isDefined(event);
      assert.strictEqual(state.toString(10), '2');
    });

    it('should distribute funds', async () => {
      const stateBefore = await instance.state.call();
      assert.strictEqual(stateBefore.toString(10), '0');

      const spender1before = await web3.eth.getBalance(spender1.address);
      const spender2before = await web3.eth.getBalance(spender2.address);
      const depositAmount = '3124123874698734592';

      await instance.deposit(accounts[0], { value: depositAmount });
      await instance.close();
      const spender1balance = await web3.eth.getBalance(spender1.address);
      const spender2balance = await web3.eth.getBalance(spender2.address);
      const spender1diff = spender1balance.sub(spender1before);
      const spender2diff = spender2balance.sub(spender2before);

      assert.strictEqual(
        spender1diff.toString(10), '2186886712289114215');
      assert.strictEqual(
        spender2diff.toString(10), '937237162409620377');
      assert.isTrue(spender1diff.plus(spender2diff).eq(depositAmount));
      assert.strictEqual(web3.eth.getBalance(instance.address).toString(10), '0');
    });
  });

  describe('enableRefunds', () => {
    it('should toggle refundsEnabled to true', async () => {
      await instance.enableRefunds();
      const state = await instance.state.call();
      const event = await utils.getLatestEvent(instance, 'RefundsEnabled');

      assert.isDefined(event);
      assert.strictEqual(state.toString(10), '1');
    });
  });

  describe('refund', () => {
    const donor = accounts[1];

    it('should throw if caller has not donated', async () => {
      await instance.enableRefunds();
      return utils.shouldThrow(instance.refund.bind(instance, accounts[0]));
    });

    it('should throw if caller has already been refunded', async () => {
      await instance.deposit(donor, { value: web3.toWei(1) });
      await instance.enableRefunds();
      await instance.refund(donor, { from: donor });
      return utils.shouldThrow(instance.refund.bind(instance, donor, { from: donor }));
    });

    it('should throw if refunds have not been enabled', async () => {
      await instance.deposit(donor, { value: web3.toWei(1) });
      return utils.shouldThrow(instance.refund.bind(instance, donor, { from: donor }));
    });

    it('should refund user', async () => {
      const amount = web3.toWei(1);
      await instance.deposit(donor, { value: amount });
      await instance.enableRefunds();
      const balanceBefore = await web3.eth.getBalance(donor);
      const tx = await instance.refund(donor, { from: donor, gas: 250000, gasPrice: 1 });
      const txCost = tx.receipt.gasUsed;
      const balanceAfter = await web3.eth.getBalance(donor);
      const event = await utils.getLatestEvent(instance, 'Refunded');

      assert.strictEqual(event.beneficiary, donor);
      assert.strictEqual(event.weiAmount.toString(10), amount);
      assert.strictEqual(
        balanceBefore.plus(amount).minus(txCost).toString(10), balanceAfter.toString(10));
    });
  });
});

contract('Vault', (accounts) => {
  describe('constructor', () => {
    it('should throw if secondary wallet is null address', () => (
      utils.shouldThrow(Vault.new.bind(Vault, accounts[0], 0))));

    it('should throw if wallet is null address', () => (
      utils.shouldThrow(Vault.new.bind(Vault, 0, accounts[0]))));
  });
});
