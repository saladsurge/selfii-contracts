const Whitelist = artifacts.require('Whitelist');
const utils = require('./utils');

contract('Whitelist', (accounts) => {
  let instance;

  beforeEach(async () => {
    instance = await Whitelist.new();
  });

  describe('preconditions', () => {
    it('public cap should be zero', async () => {
      const publicCap = await instance.publicCap.call();
      assert.strictEqual(publicCap.toNumber(), 0);
    });
  });

  describe('addUser', () => {
    it('should add a user with cap', async () => {
      await instance.addUser(accounts[1], 123);
      const event = await utils.getLatestEvent(instance, 'UserAdded');
      const cap = await instance.addressCap.call(accounts[1]);

      assert.strictEqual(cap.toNumber(), 123);
      assert.strictEqual(event._user, accounts[1]);
      assert.strictEqual(event._cap.toNumber(), 123);
    });

    it('should throw if caller is not owner', () => (
      utils.shouldThrow(instance.addUser.bind(instance, accounts[1], 123, { from: accounts[1] }))));
  });

  describe('addUsers', () => {
    it('should throw if caller is not owner', () => (
      utils.shouldThrow(
        instance.addUsers.bind(
          instance, [accounts[1], accounts[2]], [123, 456], { from: accounts[1] }))));

    it('should throw if users and cap arrays are not equal length', () => {
      const users = [accounts[1], accounts[2]];
      const caps = [123];

      return utils.shouldThrow(instance.addUsers.bind(instance, users, caps));
    });

    it('should add multiple users', async () => {
      const users = [accounts[1], accounts[2]];
      const caps = [123, 456];
      await instance.addUsers(users, caps);

      const setCaps = await Promise.all(users.map(user => instance.addressCap.call(user)));
      setCaps.forEach((cap, idx) => {
        assert.strictEqual(cap.toNumber(), caps[idx]);
      });
    });
  });

  describe('setPublicCap', () => {
    it('should throw if caller is not owner', () => (
      utils.shouldThrow(instance.setPublicCap.bind(instance, 123, { from: accounts[1] }))));

    it('should set public cap', async () => {
      await instance.setPublicCap(123);
      const cap = await instance.publicCap.call();
      assert.strictEqual(cap.toString(10), '123');
    });
  });

  describe('getCap', () => {
    it('should return publicCap if user cap is 1 wei', async () => {
      const expectedCap = 123456;
      await instance.setPublicCap(expectedCap);
      await instance.addUser(accounts[1], 1);
      const cap = await instance.getCap.call(accounts[1]);
      assert.strictEqual(cap.toNumber(), expectedCap);
    });

    it('should return zero if no cap exists for given address', async () => {
      await instance.setPublicCap(4567);
      const cap = await instance.getCap.call(accounts[1]);
      assert.strictEqual(cap.toNumber(), 0);
    });

    it('should return existing cap for address', async () => {
      const expectedCap = 789;
      await instance.addUser(accounts[1], expectedCap);
      const cap = await instance.getCap.call(accounts[1]);
      assert.strictEqual(cap.toNumber(), expectedCap);
    });
  });

  describe('destroy', () => {
    it('should throw if caller is not owner', () => (
      utils.shouldThrow(instance.destroy.bind(instance, { from: accounts[1] }))));

    it('should self destruct contract', async () => {
      await instance.destroy();
      const code = await web3.eth.getCode(instance.address);
      assert.isTrue(code === '0x0' || code === '0x');
    });
  });
});
