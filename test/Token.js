const Token = artifacts.require('TimeTravellingToken');
const utils = require('./utils');

const NULL_ADDRESS = '0x0000000000000000000000000000000000000000';

contract('Token', (accounts) => {
  let token;
  const minter = accounts[1];

  beforeEach(async () => {
    token = await Token.new(minter);
  });

  describe('constructor', () => {
    it('should throw if minter address is null', () => (
      utils.shouldThrow(Token.new.bind(token, 0))));
  });

  describe('mint', () => {
    it('should throw when caller is not minter', () => (
      utils.shouldThrow(token.mint.bind(
        token, minter, 123, { from: accounts[0] }))));

    it('should throw when minting is finished', async () => {
      await token.finishMinting({ from: minter });
      return utils.shouldThrow(
        token.mint.bind(token, accounts[2], 123, { from: minter }));
    });

    it('should issue tokens to requested user', async () => {
      await token.mint(accounts[2], 123, { from: minter });

      const balance = await token.balanceOf.call(accounts[2]);
      const event = await utils.getLatestEvent(token, 'Mint');
      const supply = await token.totalSupply.call();

      assert.strictEqual(balance.toNumber(), 123);
      assert.strictEqual(event.recipient, accounts[2]);
      assert.strictEqual(event.amount.toNumber(), 123);
      assert.strictEqual(supply.toNumber(), 123);
    });
  });

  describe('transfer', () => {
    const mintAmount = 1234;
    const holder = accounts[2];

    beforeEach(async () => {
      await token.mint(holder, mintAmount, { from: minter });
    });

    it('should throw when minting is active', () => (
      utils.shouldThrow(
        token.transfer.bind(token, accounts[3], 123, { from: holder }))));

    it('should throw when recipient is null address', async () => {
      await token.finishMinting({ from: minter });
      return utils.shouldThrow(
        token.transfer.bind(token, NULL_ADDRESS, 123, { from: holder }));
    });

    it('should throw when recipient is token itself', async () => {
      await token.finishMinting({ from: minter });
      return utils.shouldThrow(
        token.transfer.bind(token, token.address, 123, { from: holder }));
    });

    it('should throw when balance is insufficent', async () => {
      await token.finishMinting({ from: minter });
      return utils.shouldThrow(
        token.transfer.bind(
          token, accounts[3], mintAmount + 1, { from: minter }));
    });

    it('should be able to send normal tokens', async () => {
      await token.finishMinting({ from: minter });
      await token.transfer(accounts[3], 12, { from: holder });
      const balanceSender = await token.balanceOf.call(holder);
      const balanceReceiver = await token.balanceOf.call(accounts[3]);

      assert.strictEqual(balanceSender.toNumber(), mintAmount - 12);
      assert.strictEqual(balanceReceiver.toNumber(), 12);

      const event = await utils.getLatestEvent(token, 'Transfer');
      assert.strictEqual(event.from, holder);
      assert.strictEqual(event.to, accounts[3]);
      assert.strictEqual(event.value.toNumber(), 12);
    });
  });

  describe('transferFrom', () => {
    const mintAmount = 1234;
    const holder = accounts[2];

    beforeEach(async () => {
      await token.mint(holder, mintAmount, { from: minter });
    });

    it('should throw when minting is active', async () => {
      await token.approve(accounts[0], 12, { from: holder });
      return utils.shouldThrow(
        token.transferFrom.bind(token, holder, accounts[3], 12));
    });

    it('should throw when recipient is null address', async () => {
      await token.finishMinting({ from: minter });
      await token.approve(accounts[0], 12, { from: holder });
      return utils.shouldThrow(
        token.transferFrom.bind(token, holder, NULL_ADDRESS, 123));
    });

    it('should throw when recipient is token itself', async () => {
      await token.finishMinting({ from: minter });
      await token.approve(accounts[0], 12, { from: holder });
      return utils.shouldThrow(
        token.transferFrom.bind(token, holder, token.address, 123));
    });

    it('should throw when balance is insufficent', async () => {
      await token.finishMinting({ from: minter });
      await token.approve(accounts[0], mintAmount, { from: holder });
      return utils.shouldThrow(
        token.transferFrom.bind(
          token, holder, accounts[3], mintAmount + 1, { from: minter }));
    });

    it('should be able to send normal tokens', async () => {
      const recipient = accounts[3];
      const spender = accounts[1];
      await token.finishMinting({ from: minter });
      await token.approve(spender, 12, { from: holder });
      await token.transferFrom(holder, recipient, 12, { from: spender });
      const balanceSender = await token.balanceOf.call(holder);
      const balanceReceiver = await token.balanceOf.call(recipient);

      assert.strictEqual(balanceSender.toNumber(), mintAmount - 12);
      assert.strictEqual(balanceReceiver.toNumber(), 12);

      const event = await utils.getLatestEvent(token, 'Transfer');
      assert.strictEqual(event.from, holder);
      assert.strictEqual(event.to, recipient);
      assert.strictEqual(event.value.toNumber(), 12);
    });
  });

  describe('finishMinting', () => {
    it('should throw when sender is not minter', () => (
      utils.shouldThrow(token.finishMinting.bind(token))));

    it('should throw when minting has already been finished', async () => {
      await token.finishMinting({ from: minter });
      return utils.shouldThrow(token.finishMinting.bind(token, { from: minter }));
    });

    it('should set mintingFinished to true', async () => {
      const valBefore = await token.mintingFinished.call();
      assert.isFalse(valBefore);
      await token.finishMinting({ from: minter });
      const valAfter = await token.mintingFinished.call();
      assert.isTrue(valAfter);
      const event = await utils.getLatestEvent(token, 'MintFinished');
      assert.isDefined(event);
    });
  });
});
